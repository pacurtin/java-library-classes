package org.totalbeginner.tutorial;

import java.util.ArrayList;

public class MyLibrary {

	String name;
	ArrayList<Book> books;
	ArrayList<Person> people;

	public MyLibrary(String name) {
		this.name=name;
		books = new ArrayList<Book>();
		people = new ArrayList<Person>();
		
		
	}

	public String getName() {
		return name;
	}

	public ArrayList<Book> getBooks() {
		return books;
	}

	public ArrayList<Person> getPeople() {
		return people;
	}

	public void addBook(Book b1) {
		this.books.add(b1);
		
	}

	public void removeBook(Book b1) {
		this.books.remove(b1);
		
	}

	public void addPerson(Person p1) {
		this.people.add(p1);
	}

	public boolean checkOut(Book b1, Person p1) {
		int booksOut =this.getBooksForPerson(p1).size(); 
		if((b1.getPerson()== null)&&(booksOut<p1.getMaximumBooks())){
			b1.setPerson(p1);
			return true;
		}
		else{
			return false;
		}
	}

	public boolean checkIn(Book b1) {
		if(b1.getPerson()!= null){
			b1.setPerson(null);
			return true;
		}
		else{
			return false;
		}
	}

	public ArrayList<Book> getBooksForPerson(Person p1) {
		ArrayList<Book> result = new ArrayList<Book>();
		for (Book aBook : this.getBooks()) {
			if((aBook.getPerson()!=null)&&(aBook.getPerson().getName().equals(p1.getName()))){
				result.add(aBook);
			}
		}
		return result;
	}

	public ArrayList<Book> getAvailableBooks() {
		ArrayList<Book> result = new ArrayList<Book>();
		for (Book aBook : this.getBooks()) {
			if(aBook.getPerson()==null){
				result.add(aBook);
			}
		}
		return result;
	}

	public ArrayList<Book> getUnavailableBooks() {
		ArrayList<Book> result = new ArrayList<Book>();
		for (Book aBook : this.getBooks()) {
			if(aBook.getPerson()!=null){
				result.add(aBook);
			}
		}
		return result;
	}

	public static void main(String[] args) {
		//create new myLibrary.
		MyLibrary testLibrary = new MyLibrary("Test Drive Library");
		Book b1 = new Book("War and Peace");
		Book b2 = new Book("Great Expectations");
		b1.setAuthor("Tolstoy");
		b2.setAuthor("Dickens");
		Person jim = new Person();
		Person sue = new Person();
		sue.setName("Sue");
		jim.setName("Jim");
		
		testLibrary.addBook(b1);
		testLibrary.addBook(b2);
		testLibrary.addPerson(sue);
		testLibrary.addPerson(jim);
		
		
		System.out.println("Just created new library");
		testLibrary.printStatus();
		System.out.println("Check out war and peace to Sue");
		testLibrary.checkOut(b1, sue);
		testLibrary.printStatus();
		
		
		
		
		
	}

	private void printStatus() {
		System.out.println("Status report"+this.toString());
		for (Book aBook : this.getBooks()) {
			System.out.println(aBook);
		}
		System.out.println("End of Status update.");
		
	}
	
	
	
}
