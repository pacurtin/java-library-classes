package org.totalbeginner.tutorial;

public class Person {
	//fields
	private String name;
	private int maximumBooks;
	
	//constructor
	public Person(){
		name="unknown name";
		maximumBooks=3;
	}
	
	//methods
	//getters
	public String getName(){
		return name;
	}
	
	public int getMaximumBooks(){
		return maximumBooks;
	}
	
	//setters
	public void setName(String nameInput){
		name=nameInput;
	}
	
	public void setMaximumBooks(int maxBooksInput){
		maximumBooks=maxBooksInput;
	}
	
	//other methods
	public String toString(){
		return this.getName()+" ("+this.getMaximumBooks()+" books)";
	}
}
